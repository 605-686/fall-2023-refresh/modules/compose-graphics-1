---
title: Compose Graphics 1
---

Building custom components with Jetpack Compose is significantly easier than it used to be with the old View approach!

In this module, we'll take a quick look at the old way and compare it to Compose, then dive in for some progressively more complex custom components, drawing everything ourselves to create exactly the UI we would like.

## Video

I really like the videos for this module, so I'm not converting them to text. I have added some
notes to some of the videos for some minor changes, but the vast majority of the content works
as-is.

Total video time for this module: 1:47:55

!!! note

    You can change the quality and playback speed by clicking the :material-cog: icon when the video is playing. 

    If you would like to search the captions for this video, click _Watch on YouTube_, press "..." and choose _Show Transcript_.

### Jetpack Compose Graphics: Introduction (Fall 2021) (06:50)

<iframe width="800" height="450" src="https://www.youtube.com/embed/1t41a-X8Ewc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: A Simple Gauge (Fall 2021) (24:32)

!!! note

    A few notes on the content of this video:

    14:55 - Note that `Offset`` is a `value class` in Kotlin. It's represented as a single `Long` value. A `value class` allows us to treat this `Long` as an object and call functions on it.
    The x and y values are packed together in that `Long`.
    Because we're not allocating an object, we don't need to worry about
    extra heap overhead or garbage collection, which is important in
    the `Canvas.onDraw` lambda.
    
    16:50 - `Offset.Zero` is a singleton "instance" of `Offset` for the common (0f, 0f) case.

<iframe width="800" height="450" src="https://www.youtube.com/embed/BpVQus2r4qs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: An Analog Clock (Fall 2021) (20:00)

<iframe width="800" height="450" src="https://www.youtube.com/embed/5FtqH8dv0BM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Jetpack Compose Graphics: Graph Editor, Part 1 (Fall 2021) (56:33)

!!! note

    A few notes on the content of this video:

    16:13 - `Size`` is a `value class` as well, so we didn't really need the `remember`
            (we're not creating an object)

    28:45 - The naming convention should really be `_shapes`` - I got it backwards when I
            recorded this video

    37:24 - We should now use `collectAsStateWithLifecycle`, which was released after I
            recorded the video. This requires dependency 
            `"androidx.lifecycle:lifecycle-runtime-compose:2.6.2"`

<iframe width="800" height="450" src="https://www.youtube.com/embed/kNLr8Oe8vnA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Example Source

[https://gitlab.com/605-686/fall-2023-refresh/modules/compose-graphics-1](https://gitlab.com/605-686/fall-2023-refresh/modules/compose-graphics-1)
