package com.androidbyexample.oldcustomview

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.androidbyexample.oldcustomview.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        lifecycleScope.launch(Dispatchers.Default) {
            var colorNum = 0
            var value = 0f
            while(true) {
                value += 0.1f
                withContext(Dispatchers.Main) {
                    binding.custom1.value = value.coerceIn(0f, 1f)
                    binding.custom1.fillColor =
                        when (colorNum) {
                            0 -> Color.BLUE
                            1 -> Color.GREEN
                            else -> Color.RED
                        }
                }
                colorNum = (colorNum+1) % 3
                delay(1000)
            }
        }
    }
}